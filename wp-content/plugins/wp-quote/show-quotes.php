<?php

add_shortcode( 'show-quotes', 'show_quotes_list' ); 

function show_quotes_list( $atts ){
		global $wpdb;
		$table_name = $wpdb->prefix . "quotes";
        $srcQuote = $_GET['search_qn'];

		$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
        $limit = 10;
        $offset = ($pagenum-1) * $limit;

        if( isset($_GET['search_qn']) && $_GET['search_qn'] !="" ){
        	$count_query = "SELECT COUNT(*) FROM ".$table_name." WHERE quote_number LIKE '%".$srcQuote."%'";
        }else{
        	$count_query = "SELECT COUNT(*) FROM ".$table_name;
        }

        $total = $wpdb->get_var( $count_query );
        $num_of_pages = ceil( $total / $limit );

        if( isset($_GET['search_qn']) && $_GET['search_qn'] !="" ){
        	
        	$qry="select * from ". $table_name." WHERE quote_number LIKE '%".$srcQuote."%' LIMIT ". $offset .", " .$limit;
        }else{
        	$qry="select quote_title, quote_name, quote_date, quote_quantity, quote_description, quote_number from $table_name LIMIT $offset, $limit";
        }


        $result=$wpdb->get_results($qry, object);

        	$form ='<form method="get" class="search-form">';
        	$form .='<input class="search-item" type="text" name="search_qn" placeholder="Search Quote Number">';
        	$form .='<input class="search-submit" type="submit" value="Search">';
        	$form .='</form>';

        	echo $form;

        if($result){
        //var_dump($result);

        	echo '<table id="quotesTable"><tr class="quote-list">';
        	$item_name = '<td class="quote-item">Title</td>';
        	$item_name .= '<td class="quote-item">Name</td>';
        	$item_name .= '<td class="quote-item">Date</td>';
        	$item_name .= '<td class="quote-item">quantity</td>';
        	$item_name .= '<td class="quote-item">Description</td>';
        	$item_name .= '<td class="quote-item">Quote Number</td> </tr>';
        	echo $item_name;
            foreach($result as $row){
            		$list_item = '<tr class="quote-list"><td class="quote-item">'.$row->quote_title .'</td>';
            		$list_item .= '<td class="quote-item">'.$row->quote_name .'</td>';
            		$list_item .= '<td class="quote-item">'.$row->quote_date .'</td>';
            		$list_item .= '<td class="quote-item">'.$row->quote_quantity .'</td>';
            		$list_item .= '<td class="quote-item">'.$row->quote_description .'</td>';
            		$list_item .= '<td class="quote-item">'.$row->quote_number .'</td></tr>';
                echo $list_item;
            }
            echo "</table>";

            $page_links = paginate_links( array(
                'base'               => add_query_arg( 'pagenum', '%#%' ),
                'format'             => '',
                'prev_text'          => __( '&laquo;', 'aag' ),
                'next_text'          => __( '&raquo;', 'aag' ),
                'total'              => $num_of_pages,
                'current'            => $pagenum,               
                'base'               => add_query_arg( 'pagenum', '%#%' ),
                'format'             => '',
                'prev_next'          => true,
                'prev_text'          => __( '&larr;', 'aag' ),
                'next_text'          => __( '&rarr;', 'aag' ),
                'before_page_number' => '<li><span class="page-numbers btn btn-pagination btn-tb-primary">',
                'after_page_number'  => '</span></li>'
            ) );
            if ( $page_links ) {
                ?>
                <br class="clear">
            <nav id="archive-navigation" class="paging-navigation tbWow fadeInUp" role="navigation" style="visibility: visible; animation-name: fadeInUp;">
                <ul class="page-numbers">
                    <?php echo $page_links; ?>
                </ul>
            </nav>
        <?php   }


    }else{
        
        	_e ("<h2>There is no quotes found according to the quote number.</h2>");
    }

        //endif;      

       ?>
       
<script>
	$(document).ready( function () {
		    $('#quotesTable').DataTable();
		} );
</script>
       <?php 


}