<?php 
/**
 * Plugin Name:  WP Quote
 * Plugin URI: 
 * Description: This plugin use for Share quote.
 * Version: 1.0
 * Author: Iqbal Hasan
 * Author URI: 
 */

/*=== Create Table after installation =======*/
global $wpdb;


/*Datatable Css and Js*/
function add_datatables_scripts() {
  wp_register_script('datatables', 'https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js', array('jquery'), true);
  wp_enqueue_script('datatables');

  wp_register_script('datatables_bootstrap', 'https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js', array('jquery'), true);
  wp_enqueue_script('datatables_bootstrap');
}
 
function add_datatables_style() {
  wp_register_style('bootstrap_style', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
  wp_enqueue_style('bootstrap_style');

  wp_register_style('datatables_style', 'https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css');
  wp_enqueue_style('datatables_style');
}
 
add_action('wp_enqueue_scripts', 'add_datatables_scripts');
add_action('wp_enqueue_scripts', 'add_datatables_style');


/*Custom Css and Js*/
add_action('init', 'register_script');
function register_script() {
    wp_register_script( 'customjs', plugins_url('/_includes/assets/js/customjs.js', __FILE__), array('jquery'), '2.5.1' );
    wp_register_style( 'style', plugins_url('/_includes/assets/css/style.css', __FILE__), false, '1.0.0', 'all');
}

add_action('wp_enqueue_scripts', 'enqueue_style');

function enqueue_style(){
   wp_enqueue_script('customjs');
   wp_enqueue_style( 'style' );
}






function wp_quote_installer(){   

 include( plugin_dir_path( __FILE__ ) . '/installer.php');

}
register_activation_hook(__file__, 'wp_quote_installer');


 include( plugin_dir_path( __FILE__ ) . '/show-quotes.php');




