<?php
global $wpdb;
$table_name = $wpdb->prefix . "quotes";
$quotes_db_version = `1.0`;
$charset_collate = $wpdb->get_charset_collate();

if ( $wpdb->get_var("SHOW TABLES LIKE `{$table_name}`") != $table_name ) {
	
    $sql = "CREATE TABLE $table_name (
            ID int NOT NULL AUTO_INCREMENT,
            quote_title varchar(250) NOT NULL,
            quote_name varchar(250) NOT NULL,
            quote_date DATE NOT NULL,
            quote_quantity int NOT NULL,
            quote_description longtext NOT NULL,
            quote_number varchar(200) NOT NULL,
            PRIMARY KEY  (ID)
    ) $charset_collate;";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    add_option('db_quote_version', $quotes_db_version);
}



/*
$installed_ver = get_option( "db_quote_version" );

if ( $installed_ver != $jal_db_version ) {

	$table_name = $wpdb->prefix . `liveshoutbox`;

	$sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		time datetime DEFAULT `0000-00-00 00:00:00` NOT NULL,
		name tinytext NOT NULL,
		text text NOT NULL,
		url varchar(100) DEFAULT `` NOT NULL,
		PRIMARY KEY  (id)
	);";

	require_once( ABSPATH . `wp-admin/includes/upgrade.php` );
	dbDelta( $sql );

	update_option( "jal_db_version", $jal_db_version );
}*/