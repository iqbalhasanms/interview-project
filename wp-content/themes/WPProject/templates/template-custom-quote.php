<?php
/**
 * Template Name: Custom Quote Template
 * Template Post Type: Page
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

get_header();
?>

<div class="container-fluid">

<?php
        if (!empty($_POST)) {
       // require_once('/wp-config.php');	
        global $wpdb;
             $table_name = $wpdb->prefix . "quotes";            
			$quote_array = array(
	            'quote_title' 			=> $_POST['quote_title'], 
	            'quote_name' 			=> $_POST['quote_name'], 
	            'quote_date'   			=> $_POST['quote_date'],
	            'quote_quantity'   		=> $_POST['quote_quantity'],
	            'quote_description'   	=> $_POST['quote_description'],
	            'quote_number'   		=> $_POST['quote_number']);


      $success = $wpdb->insert($table_name, $quote_array, $formet = NULL);

            if($success){
				echo '<h3>Your quote successfully submited to our Organization! Our Staff will contact you!</h3>' ; 
				?>

				
				<?php
			}
		}
		else   { ?>

			    <div class="col-sm-offset-1 col-sm-8">
			    	<h2> Create Your Quote </h2>

				    <form class="form-horizontal" method = "post">				    
				    <div class="form-group">
				      <label class="control-label col-sm-4" for="quote_title">Quote Title:</label>
				      <div class="col-sm-8">          
				        <input type="text" class="form-control" id="quote_title" placeholder="Enter Quote Title" name="quote_title">
				      </div>
				    </div>

				    <div class="form-group">
				      <label class="control-label col-sm-4" for="quote_name">Quote Name:</label>
				      <div class="col-sm-8">          
				        <input type="text" class="form-control" id="quote_name" placeholder="Enter Quote Name" name="quote_name">
				      </div>
				    </div>

				    <div class="form-group">
				      <label class="control-label col-sm-4" for="quote_date">Quote Date:</label>
				      <div class="col-sm-8">          
				        <input type="date" class="form-control" id="quote_date" placeholder="Enter Date" name="quote_date">
				      </div>
				    </div>


				    <div class="form-group">
				      <label class="control-label col-sm-4" for="quote_quantity">Quote Quantity:</label>
				      <div class="col-sm-8">          
				        <input type="text" class="form-control" id="quote_quantity" placeholder="Enter Quantity" name="quote_quantity">
				      </div>
				    </div>

				    <div class="form-group">
				      <label class="control-label col-sm-4" for="quote_description">Quote Description:</label>
				      <div class="col-sm-8">          
				        <input type="text" class="form-control" id="quote_description" placeholder="Enter Quote Description" name="quote_description">
				      </div>
				    </div>

				    <div class="form-group">
				      <label class="control-label col-sm-4" for="quote_number">Quote Number:</label>
				      <div class="col-sm-8">          
				        <input type="text" class="form-control" id="quote_number" placeholder="Enter Quote Number" name="quote_number">
				      </div>
				    </div>

				    <div class="form-group">				      
				      <div class="col-sm-offset-4 col-sm-8">          
				        <input type="submit" class="form-control" id="quote_data_submit" name="quote_data">
				      </div>
				    </div>
				  </form>
			    	
			    </div>

	<?php }  ?>

</div>
<?php get_footer(); ?>
